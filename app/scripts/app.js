'use strict';

/**
 * App Entry point
 * 
 * Library definitions are placed here.
 */
angular.module('Chatty', ['ngResource', 'ngCookies', 'ionic', 'Chatty.services', 'Chatty.controllers']);

